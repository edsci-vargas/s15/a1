console.log("Hello World");

const details = {
	firstName: "Mingyu",
	lastName: "Kim",
	age: "25",
	hobbies: [
		"listening music", "eating", "sleeping"],
	workAddress: {
		housenumber: "Block 406 Lot 17",
		street: "Anyang",
		city: "Gyeonggi-do",
		state: "South Korea"
		}
}
const work =
Object.values(details.workAddress);
console.log("My First Name is " + details.firstName)
console.log("My Last Name is " + details.lastName)
console.log('Yes, I am ${details.firstName} ${details.lastName}.')
console.log("I am " + details.age + " years old.")
console.log('My hobbies are ${details.hobbies.join(', ')}. ');
console.log("I work at " + work.join (" , ") + ".");
